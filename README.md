# Simple markov chain sentence generator

This was made because I was bored and had learned about markov chains in my class. It's very simple and it's main purpose is to make funny sentences

## Usage

Create a message.txt file and put it in the same directory as the script.

## Configuration

You can change the order to make the generated message less or more chaotic. The default is 3.

You can change the number of states to generate. Each state contributes with order times characters. The default is 5000.
